package cse360assign2;
//////////////////////////////////////////////
//Name: Emilia Ferreyra
//Class ID: 349
//Assignment 2: creates an array with the ability 
//to add, remove, count, search, and print out values.
//////////////////////////////////////////////

public class SimpleList {
    public int[] list;
    public int count;
    
    public static void main(String[] args) {
    	SimpleList sl = new SimpleList();
    	sl.add(1);
    	sl.add(1);
    	sl.add(1);
    	sl.add(1);
    	sl.add(1);
    	sl.add(1);
    	sl.add(1);
    	sl.add(1);
    	sl.add(1);
    	sl.add(1);
    	System.out.println(sl.size());
    	System.out.println(sl.toString());
    	
    	sl.add(2);
    	System.out.println(sl.size());
    	System.out.println(sl.toString());
    	
    	sl.add(2);
    	sl.add(2);
    	sl.add(2);
    	sl.add(2);
    	System.out.println(sl.size());
    	System.out.println(sl.toString());
    	
    	sl.append(3);
    	System.out.println(sl.size());
    	System.out.println(sl.toString());
    	
    	System.out.println(sl.last());
    	
    	sl.remove(1);
    	System.out.println(sl.count());
    	System.out.println(sl.size());
    	System.out.println(sl.toString());
    	
    	sl.remove(2);
    	System.out.println(sl.count());
    	System.out.println(sl.size());
    	System.out.println(sl.toString());
    	
    	System.out.println(sl.first());
    }

    //initializes a SimpleList object with an empty array of size 10
    //and a count of 0.
    public SimpleList()
    {
        list = new int[10];
        count = 0;
    }

    //adds given integer to front of array and moves the rest of 
    //the values down one index. If the array is full, the list
    //will increase in size by 50%
    public void add(int num)
    {
    	int[] tempList; //create a temporary list to copy values into
    	
    	//if the number of values already in the list
    	//is equal to the length of the list,
    	//then set the size of tempList to list's length + half of list's length
    	if(count == list.length)
    	{
    		tempList = new int[(list.length)+(list.length / 2)];
    	}
    	//else set size of tempList to list's length
    	else
    	{
    		tempList = new int[list.length];
    	}
    	
    	tempList[0] = num; //set first value of tempList to given num
    	
    	//increment through stored values in list and add them to tempList
    	for(int i = 0; i < count; i++)
    	{
    		tempList[i+1] = list[i];
    	}
    		
    	//set tempList equal to list again
    	list = tempList;
    	
        count++; //increment count once value is added
    }

    //adds given integer to end of array. If the array is full, the list
    //will increase in size by 50%
    public void append(int num)
    {
    	int[] tempList; //create a temporary list to copy values into
    	
    	//if the number of values already in the list
    	//is equal to the length of the list,
    	//then set the size of tempList to list's length + half of list's length
    	if(count == list.length)
    	{
    		tempList = new int[(list.length)+(list.length / 2)];
    	}
    	//else set size of tempList to list's length
    	else
    	{
    		tempList = new int[list.length];
    	}
    	
    	//increment through stored values in list and add them to tempList
    	for(int i = 0; i < count; i++)
    	{
    		tempList[i] = list[i];
    	}
    	
    	//add num to end of tempList
    	tempList[count] = num;
    	
    	//set tempList equal to list again
    	list = tempList;
    	
        count++; //increment count once value is added
    }
    
    //removes all instances of a given integer from an array. 
    //All values after deleted value are moved up one index.
    public void remove (int num)
    {
        //do not continue to search for and remove zeros
        //if 0 is not an added value (zero's index > count)
        if(num != 0 || search(num) < count)
        {
            //continue searching for instances of num until
            //there are none left
            while(search(num) != -1)
            {
                for(int i = search(num); i < count-1; i++)
                {
                    //starting at the index of num, make the
                    //index equal to the value at the index
                    //after it
                    list[i] = list[i+1];
                }
                
                //list[count-1] does not have a list[i+1] to be set equal to
                //especially is count == list.length
                //so list[count-1] is set to 0 outside of for loop
                list[count-1] = 0;
                count--; //decrement count once value is removed
            }
            
            //if empty spaces are more than 25% of list, shrink list by 25%
            if((list.length - count) > (list.length / 4))
            {
            	// initialize tempList to 3/4 or 25% smaller than list
            	int[] tempList = new int[(list.length * 3)/4];
            	
            	//copy list into tempList
            	for(int i = 0; i < count; i++)
            	{
            		tempList[i] = list[i];
            	}
            	
            	//set list equal to tempList
            	list = tempList;
            }
        }
    }

    //returns the count or number of values in the array
    public int count()
    {
        return count;
    }

    //returns first value in list
    public int first()
    {
    	if(list.length > 0)
    	{
    		return list[0];
    	}
    	return -1;
    }
    
    //returns last value in list
    public int last()
    {
    	if(list.length > 0)
    	{
    		return list[count-1];
    	}
    	return -1;
    }
    
    //returns size or length of list
    public int size()
    {
    	return list.length;
    }
    
    //returns a string of all the values in the array, 
    //separated by a space
    public String toString()
    {
        //empty string to put list values into
        String listS = "";
        
        //prints only values in list that were added,
        //which is the amount of count
        for(int i = 0; i < count; i++)
        {
            //add value at list[i] to listS String
            listS += list[i];
            
            //if it is not the last added value, then
            //add a space after
            if(i < count-1)
            {
                listS += " ";
            }
        }

        //return string of all values in array,
        //separated by a space
        return listS;
    }

    //returns the index of the first instance of a given number
    //in the array. It the given value is not in the array, return -1
    public int search(int num)
    {
        //search all added values (values with indices 
        //under count)
        for(int i = 0; i < count; i++)
        {
            //if the value at list[i] is equal to num,
            //return that index (i)
            if(list[i] == num)
            {
                return i;
            }
        }
        //if num was not equal to any value in list, return -1
        return -1;
    }
    
    
}
